## 创建一个股票代码的实体类，包括股票代码、股票名称、股票价格等属性，并实现一个方法来获取股票的价格。
import pandas as pd
import user_config as ucfg
import func
from func_TDX import *
import talib
import os
class StockList:
    def __init__(self):
        self.file_listsh = ['600031']
        self.file_listsz = []
        # self.load()

    def load(self):
        # 读取通达信正常交易状态的股票列表。infoharbor_spec.cfg退市文件不齐全，放弃使用
        tdx_stocks = func.get_TDX_stock_code_list()
        # 排除不能交易的科创板688和创业板30
        self.file_listsh = tdx_stocks[0][tdx_stocks[0].apply(lambda x: x[0:1] == "6" and x[0:3] != '688')]
        self.file_listsz = tdx_stocks[0][tdx_stocks[0].apply(lambda x: x[0:1] != "6" and x[0:2] != '30')]

        # 需要剔除的通达信的概念和行业
        ignore_tdx_gainian = ["ST板块", "次新股"]  # list类型。通达信软件中查看“概念板块”。
        ignore_tdx_hangye  = ["T1002", ]  # list类型。记事本打开 通达信目录\incon.dat，查看#TDXNHY标签的行业代码。T1002=证券
        # 获取df中blockname列的值是ST板块的行，对应code列的值，转换为list。用filter函数与stocklist过滤，得出不包括ST股票的对象，最后转为list
        df = func.get_TDX_blockfilecontent("block")
        kicklist = set()
        for i in ignore_tdx_gainian:
            for code in df.loc[df['blockname'] == i]['code'].tolist():
                kicklist.add(code)
        print(f'排除概念股票{ignore_tdx_gainian} {len(kicklist)}只')

        print(f'剔除通达信行业股票: {ignore_tdx_hangye}')
        df = func.get_TDX_hangyefilecontent("hangye")
        for i in ignore_tdx_hangye:
            for code in df.loc[df[2] == i][1].tolist():
                kicklist.add(code)
        print(f'排除概念和行业股票{ignore_tdx_gainian} {len(kicklist)}只')

        with open('../util_docs/skip_stock_code.txt', 'r') as f:
            j = f.read().split(',')
            print(f'排除流通市值小于10亿的股票{len(j)}只')
            kicklist = kicklist.union(set(j))

        self.file_listsh = pd.Series([x for x in self.file_listsh if x not in kicklist])
        self.file_listsz = pd.Series([x for x in self.file_listsz if x not in kicklist])
        print(f'生成沪深股票列表, 共 {len(self.file_listsh)} 只股票')
        print(f'生成深圳股票列表, 共 {len(self.file_listsz)} 只股票')

    def pre_fix(self,stockCode,start_date='20160101',end_date='20240301'):
        path = os.path.join(ucfg.csv_lday,stockCode +'.csv')
        if not os.path.exists(path):
            return
        data = pd.read_csv(path, encoding="GBK")
        data['date'] = data['date'].str.replace('-', '')

        if start_date != '':
            if end_date != '':
                data = data[(data['date'] >= start_date) & (data['date'] <= end_date)]
            else:
                data = data[(data['date'] >= start_date)]
            # 重新设置索引从1开始
            # data.reset_index(drop=True, inplace=True)
            data.set_index('date', drop=False, inplace=True)  # 时间为索引。方便与另外复权的DF表对齐合并
        data['percent'] = ((data['close'] / REF(data['close'], 1) - 1) * 100).round(2)
        data['MA5'] = (talib.MA(data['close'],5)).round(2)
        data['MA10'] = (talib.MA(data['close'],10)).round(2)
        data['MA20'] = (talib.MA(data['close'],20)).round(2)
        data['MA60'] = (talib.MA(data['close'],60)).round(2)
        data['VOL5'] = (talib.MA(data['vol'], 5)/10000).round(2)
        data['VOL60'] = (talib.MA(data['vol'], 60)/10000).round(2)
        data['VOL100'] = (talib.MA(data['vol'], 100)/10000).round(2)
        data['MA60_MA20'] = (data['MA60']/data['MA20']*100 - 100).round(2)
        data['open'] = (data['open'] ).round(2)
        data['close'] = (data['close'] ).round(2)
        data['low'] = (data['low'] ).round(2)
        data['high'] = (data['high'] ).round(2)
        data.to_pickle( f'{ucfg.pre_pkl}{stockCode}.pkl')


if __name__ == '__main__':
    a = StockList()
    a.load()
    # p = pd.read_pickle(f'{ucfg.pre_pkl}600031.pkl')
    for c in a.file_listsh:
        a.pre_fix(c)
    #
    for c in a.file_listsz:
        a.pre_fix(c)

