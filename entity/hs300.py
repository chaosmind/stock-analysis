
import user_config as uscg
from func_TDX import *
import os
import talib
import numpy as np
import statistics
import sys
import multiprocessing, time

def read_hs300():
    df_hs300 = pd.read_csv(uscg.csv_index + '/000300.csv', index_col=None, encoding='gbk', dtype={'code': str})
    df_hs300['date'] = df_hs300['date'].str.replace('-', '')

    # start_date = f'{year-1}0101'
    # end_date = f'{year+1}0301'
    #
    start_date = f'20160101'
    end_date = f'20240101'

    if start_date != '':
        if end_date != '':
            df_hs300 = df_hs300[(df_hs300['date'] >= start_date) & (df_hs300['date'] <= end_date)]
        else:
            df_hs300 = df_hs300[(df_hs300['date'] >= start_date)]
        # 重新设置索引从1开始
        # df_hs300.reset_index(drop=True, inplace=True)

    df_hs300.set_index('date', drop=False, inplace=True)  # 时间为索引。方便与另外复权的DF表对齐合并
    df_hs300['MA5'] = talib.MA(df_hs300['close'], 5)
    df_hs300['MA20'] = talib.MA(df_hs300['close'], 20)
    df_hs300['MA60'] = talib.MA(df_hs300['close'], 60)
    df_hs300['MA20_MA60'] = (df_hs300['MA60'] - df_hs300['MA20']) /  df_hs300['MA20'] * 100

    return df_hs300

