#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
用户设置文件

作者：wking [http://wkings.net]
"""

# 配置部分开始
debug = True  # 是否开启调试日志输出  开=True  关=False

# 目录最好事先手动建立好，不然程序会出错
tdx = {
    'tdx_path': 'D:/project/TDXdata',  # 指定通达信目录
    'csv_lday': 'd:/project/TDXdata/lday_qfq',  # 指定csv格式日线数据保存目录
    'pickle': 'd:/project/TDXdata/pickle',  # 指定pickle格式日线数据保存目录
    'csv_index': 'd:/project/TDXdata/index',  # 指定指数保存目录
    'csv_cw': 'd:/project/TDXdata/cw',  # 指定专业财务保存目录
    'csv_gbbq': 'd:/project/TDXdata',  # 指定股本变迁保存目录
    'pytdx_ip': '218.6.170.55',  # 指定pytdx的通达信服务器IP
    'pytdx_port': 7709,  # 指定pytdx的通达信服务器端口。int类型
}

# tdx['tdx_path'] ='D:/软件/通达信金融终端开心果整合版'
tdx['sz_lday'] = tdx['tdx_path'] + '/vipdoc/sz/lday/'
tdx['sh_lday'] = tdx['tdx_path'] + '/vipdoc/sh/lday/'

sz_lday = tdx['sz_lday']
sh_lday = tdx['sh_lday']
csv_lday = tdx['csv_lday']
csv_index = tdx['csv_index']
csv_cw = tdx['csv_cw']
csv_gbbq= tdx['csv_gbbq']
pre_pkl = tdx['tdx_path'] + '/pre_pkl/'

index_list = [  # 通达信需要转换的指数文件。通达信按998查看重要指数
    'sh999999.day',  # 上证指数
    'sh000300.day',  # 沪深300
    'sz399001.day',  # 深成指
]
# 配置部分结束

# 策略配置参数
strategy_params = {
    "max_buy_count":4,  #最多买点次数
    "pattern_type":1,   #顶底分型的形态
    "ma5_flag": True,   #是否使用均线过滤
    "cost_percent":5,   #最多亏损，超过就核按钮，以收盘价为准，以百分比计算
}