import re
import statistics,numpy
import time,os
import os
import user_config as uscg
import pandas as pd

# 提取的信息顺序为：成功率、次数、成功数量、盈利、亏损、盈亏比、不考虑胜率的平均盈亏比、平均盈利、平均亏损

keys = ['代码','成功率', '次数', '成功数量', '盈利', '亏损', '盈亏比', '平均天数', '平均盈利', '平均亏损']
# print(matches)
# 将信息组成字典
a = []
prof_list = []
loss_list = []
prof_rate_list = []
succ_list = []
total_list = []
day_list = []
avg_prof_list = []
avg_loss_list = []
var_prof = []
var_loss = []

def stat_txt():
    txts = ['info17-24-4.txt', 'info19-24-5.txt', 'info19-24-4.txt', 'info19-24-3.5.txt', 'info19-24-3.txt', 'info19-24-2-4.txt']
    t = time.time()
    with open('../info/info1.txt', 'r', encoding='UTF8') as f:
        text = f.read()
    # 定义正则表达式模式来匹配整个文本中的数字信息
    pattern = r'(\d+),成功率为([\d.]+)，共计次数(\d+)，成功数量为(\d+),盈利([\d.]+)%,亏损([\d.]+)%,盈亏比([\d.]+),平均天数([\d.]+),平均盈利([\d.]+)%,平均亏损([\d.]+)%'
    # 使用正则表达式找到所有匹配的数字信息
    matches = re.findall(pattern, text)
    info_list = []
    code_set = set()
    for matche in matches:
        info_dict = {keys[i]: float(matche[i]) for i in range(len(keys))}
        info_list.append(info_dict)
        code_set.add(info_dict['代码'])
        # print(info_dict)
        a.append(a)
        prof_list.append(info_dict['盈利'])
        loss_list.append(info_dict['亏损'])
        prof_rate_list.append(info_dict['盈亏比'])
        succ_list.append(info_dict['成功数量'])
        total_list.append(info_dict['次数'])
        day_list.append(info_dict['平均天数'])
        avg_prof_list.append(info_dict['平均盈利'])
        avg_loss_list.append(info_dict['平均亏损'])
        var_prof.extend([info_dict['平均盈利'] for x in range(0, int(succ_list[-1]))])
        var_loss.extend([info_dict['平均亏损'] for x in range(0, int(loss_list[-1]))])

    print(f"代码数量{len(code_set)}")
    print(f'成功数量{sum(succ_list)}')
    print(f'次数{sum(total_list)}')
    print(f'成功率{sum(succ_list) / sum(total_list) * 100:.1f}%')
    print(f'盈利{sum(prof_list):.2f}')
    print(f'亏损{sum(loss_list):.2f}')
    print(f'盈亏比{sum(prof_list) / sum(loss_list):.2f}')
    print(f'单次平均盈利{(sum(prof_list) - sum(loss_list)) / sum(total_list):.2f}')

    print(f'平均天数{sum(day_list) / len(day_list):.2f}')
    print(f'平均盈利{sum(avg_prof_list) / len(avg_prof_list):.2f}')
    print(f'平均亏损{sum(avg_loss_list) / len(avg_loss_list):.2f}')
    # print(f'平均盈利标准差{numpy.std(var_prof):.2f}')
    # print(f'平均亏损标准差{numpy.std(var_loss):.2f}')

    # 计算盈利和亏损的方差
    print(f'运行时间:{time.time() - t:.3f}s')

def comp(info,result):
    stockCode = f"{int(info['代码']):06d}"
    result = result[result['stock_code'] == stockCode]
    if len(result) == 0:
        pass
    prof_list = result[result['price_percent'] > 0]
    loss_list = result[result['price_percent'] < 0]
    prof_sum =  prof_list['price_percent'].sum().round(2)
    loss_sum =  abs(loss_list['price_percent'].sum()).round(2)
    succ_rate = len(prof_list) / len(result) * 100 if len(result) > 0 else 0
    if prof_sum != info['盈利'] or loss_sum != info['亏损']:
        print(f'{stockCode}成功数量{len(prof_list)}，次数{len(result)}，成功率{succ_rate:.1f}%，盈利{prof_sum:.2f}，亏损{loss_sum:.2f}')
        print(f"{stockCode}盈利{info['盈利']:.2f}，亏损{info['亏损']:.2f}")

    if len(prof_list) != info['成功数量'] or len(result) != info['次数']:
        print(f'{stockCode}成功数量{len(prof_list)}，次数{len(result)}')


def load_from_new_data(index=0,timestr=None):
    data = pd.DataFrame()
    if timestr is not None:
        filenames = [f'{timestr}/result_{timestr}_2022_{str(x)}.pkl' for x in range(0, 5)]
        for filename in filenames:
            read_result = pd.read_pickle(f'../result/{filename}')
            data = pd.concat([data,read_result], axis=0)
    else:

        for root, dirs, files in os.walk('../result', topdown=True):
            if len(dirs) != 0:
                dirs = sorted(dirs, reverse=True)
                for root2, dirs2, files2 in os.walk(root + os.sep + dirs[index]):
                    for filename in files2:
                        if '.py' in filename or '.txt' in filename:
                            continue
                        read_result = pd.read_pickle(root2 + os.sep + filename)
                        data = pd.concat([data, read_result], axis=0)

    data['start_date'] = data['start_date'].str.replace('-', '')
    return data

def stat_add_pkl(start_year,end_year):
    # data = load_from_new_data("202403022238")
    data = load_from_new_data()
    result = data[(data['start_date'] >= f'{start_year}0101') & (data['start_date'] <= f'{end_year}0301')]
    # result = result.sort_values(by='stock_code')
    result.reset_index(inplace=True, drop=True)
    columns = ['stock_code', 'start_date', 'end_date', 'start', 'end', 'distance', 'prev_close', 'close',  'price_percent', 'start_type', 'end_type']

    cache_data = {}
    for index, value in result.iterrows():
        if value['stock_code'] in cache_data.keys():
            item_data = cache_data.get(value['stock_code'])
        else:
            item_data = pd.read_pickle(os.path.join(uscg.pre_pkl, value['stock_code'] + '.pkl'))
            cache_data[value['stock_code']] = item_data

        mid_close = 0
        for pkl_index in range(value['start']+1,value['end']):
            if item_data['close'].iloc[pkl_index] > value['prev_close']:
                mid_close = item_data['close'].iloc[pkl_index]
                break
        if mid_close != 0:
            chenben = (value['prev_close'] + mid_close) / 2.0
            result.loc[index, 'price_percent'] = ((value['close'] - chenben)/chenben*100.0)
        else:
            result.loc[index, 'price_percent'] = result.loc[index, 'price_percent'] /2

    print_result(start_year,end_year,result)
    # print_mtd_result(start_year,end_year,result)


def print_result(start_year,end_year,result):
    if len(result) == 0:
        return
    prof_list = result[result['price_percent'] > 0]
    loss_list = result[result['price_percent'] < 0]
    day_sum = result['distance'].sum()
    prof_sum = prof_list['price_percent'].sum().round(2)
    loss_sum = abs(loss_list['price_percent'].sum()).round(2)
    succ_rate = len(prof_list) / len(result) * 100 if len(result) > 0 else 0

    # 使用 NumPy 计算均值和标准差
    mean = numpy.mean(result['price_percent'].to_list())
    std_dev = numpy.std(result['price_percent'].to_list())
    std_day = numpy.std(result['distance'].to_list())

    # print(f'{stockCode}成功数量{len(prof_list)}，次数{len(result)}，成功率{succ_rate:.1f}%，盈利{prof_sum:.2f}，亏损{loss_sum:.2f}')
    avg_prof_rate = (prof_sum - loss_sum) / len(result)
    print(
        f'开始{start_year} 结束 {end_year},成功数量{len(prof_list)}，次数{len(result)}，成功率{succ_rate:.1f}%，盈利{prof_sum:.2f}，亏损{loss_sum:.2f},盈亏比{prof_sum / loss_sum:.2f}, 单次平均盈利{avg_prof_rate:.2f},'
        f'平均盈利{prof_sum / len(prof_list):.2f},平均亏损{loss_sum / len(loss_list):.2f},'
        f'平均天数{(day_sum) / len(result):.2f},天数标准差{std_day:.2f},盈利标准差{std_dev:.2f},夏普比{avg_prof_rate/std_dev*10:.4f}')

##统计一个标准差范围内的结果,进行概率统计
def print_mtd_result(start_year, end_year, result):
    if len(result) == 0:
        return
    # 使用 NumPy 计算均值和标准差
    mean = numpy.mean(result['price_percent'].to_list())
    std_dev = numpy.std(result['price_percent'].to_list())
    prof_list = result[(result['price_percent'] > 0) & (result['price_percent'] < (std_dev*2+mean))]
    loss_list = result[result['price_percent'] < 0]
    day_sum = result['distance'].sum()
    prof_sum = prof_list['price_percent'].sum().round(2)
    loss_sum = abs(loss_list['price_percent'].sum()).round(2)
    ##将盈利异常值优化成盈利平均值，毕竟系统50%的利润，来至于那5%的盈利交易，所以需要完全覆盖这些交易
    prof_sum = prof_sum + len(result) * 0.05 * prof_sum / len(prof_list)
    succ_rate = len(prof_list) / len(result) * 100 if len(result) > 0 else 0
    print(
        f'开始{start_year} 结束 {end_year},成功数量{len(prof_list)}，次数{len(result)}，成功率{succ_rate:.1f}%，盈利{prof_sum:.2f}，亏损{loss_sum:.2f},盈亏比{prof_sum / loss_sum:.2f}, 单次平均盈利{(prof_sum - loss_sum) / len(result):.2f},'
        f'平均盈利{prof_sum / len(prof_list):.2f},平均亏损{loss_sum / len(loss_list):.2f},'
        f'平均天数{(day_sum) / len(result):.2f},均值{mean:.2f},盈利标准差{std_dev:.2f}')



def stat_95_pkl(start_year,end_year):
    # data = load_from_new_data("202403022238")
    data = load_from_new_data()
    result = data[(data['start_date'] >= f'{start_year}0101') & (data['start_date'] <= f'{end_year}0301')]
    result.reset_index(inplace=True, drop=True)
    columns = ['stock_code', 'start_date', 'end_date', 'start', 'end', 'distance', 'prev_close', 'close',  'price_percent', 'start_type', 'end_type']
    import os
    import user_config as uscg
    cache_data = {}



    for index, value in result.iterrows():
        if value['stock_code'] in cache_data.keys():
            item_data = cache_data.get(value['stock_code'])
        else:
            item_data = pd.read_pickle(os.path.join(uscg.pre_pkl, value['stock_code'] + '.pkl'))
            cache_data[value['stock_code']] = item_data

        for ref_day in range(10,20):
            close_index = value['start']+ref_day
            if close_index > len(item_data) - 1:
                close_index = len(item_data) - 1

            A20_REF_close = item_data['close'].iloc[close_index-1]
            A20_close = item_data['close'].iloc[close_index]
            MA20 = item_data['MA20'].iloc[close_index]
            MA5 = item_data['MA5'].iloc[close_index]
            MA5_REF = item_data['MA5'].iloc[close_index-1]

            chenben = 0
            if A20_close > MA20 and A20_close/value['prev_close'] > 1.15 and A20_close > A20_REF_close and A20_REF_close < MA5_REF:
                chenben = A20_close
                price_percent = ((value['close'] - chenben) / chenben * 100.0)
                if price_percent < -4:
                    price_percent = -4
                result.loc[index, 'price_percent'] = price_percent
                d = value['distance'] - ref_day
                if d < 0:
                    d = 0
                result.loc[index, 'distance'] = d
                break
            else:
                result.loc[index, 'price_percent'] = 0
                result.loc[index, 'distance'] = 0


    result = result[(result['price_percent'] != 0)].copy()
    print_result(start_year,end_year,result)
    # print_mtd_result(start_year,end_year,result)


def stat_pkl(start_year,end_year):
    data = load_from_new_data(0)
    result = data[(data['start_date'] >= f'{start_year}0101') & (data['start_date'] <= f'{end_year}0101')]
    # result = result.sort_values(by='stock_code')
    result.reset_index(inplace=True, drop=True)
    columns = ['stock_code', 'start_date', 'end_date', 'start', 'end', 'distance','prev_close', 'close', 'price_percent', 'start_type', 'end_type']
    print("___________________________________________")
    print_result(start_year,end_year,result)
    print_mtd_result(start_year,end_year,result)

    from pyecharts.charts import Bar
    from pyecharts import options as opts

    # 定义区间范围和对应的计数器
    interval_ranges = [(x,x+1) for x in range(-8,30)]
    counter = [0] * len(interval_ranges)

    # 统计每个区间内出现的数据个数
    for index,value in result.iterrows():
        for i, (start, end) in enumerate(interval_ranges):
            if start <= float(value['price_percent']) < end:
                counter[i] += 1
                break

    counter = [f'{(x/len(result)*100):.2f}' for x in counter]
    over_product = 0
    for i,p in enumerate(counter):
        if i > 10:
            over_product += float(p)*(i-10)

    # Pyecharts 绘制柱状图
    bar = (
        Bar()
            .add_xaxis([f"{start}-{end}%" for start, end in interval_ranges])
            .add_yaxis("百分比统计", counter)
            .set_global_opts(title_opts=opts.TitleOpts(title="百分比数据区间统计"))
    )

    bar.render("percentages_bar_chart.html")

    distance_list = result['distance'].to_list()

    from collections import Counter
    counter = Counter(distance_list)

    # 提取数据
    numbers = sorted(list(counter.keys()))
    counts = [counter[x] for x in numbers]
    from pyecharts.charts import Bar
    from pyecharts import options as opts
    bar2 = (
        Bar()
            .add_xaxis(numbers)
            .add_yaxis("距离出现次数", counts)
            .set_global_opts(title_opts=opts.TitleOpts(title="分型间隔距离统计图"))
    )
    # 渲染图表
    bar2.render(f"../html/pattern_distances_bar_chart.html")

    # stat_3day(result)

def stat_3day(result):
    import os
    import user_config as uscg
    for index,value in result.iterrows():
        data = pd.read_pickle(os.path.join(uscg.pre_pkl, value['stock_code'] + '.pkl'))
        value_date = data[data['date'] >= value['start_date']].head(value['distance'])

def print_table(data):
    table = PrettyTable(data[0])
    for row in data[1:]:
       table.add_row(row)
    print(table)



from hs300 import read_hs300

def stat_hs300(start_year,end_year):
    data = load_from_new_data(0)
    result = data[(data['start_date'] >= f'{start_year}0101') & (data['start_date'] <= f'{end_year}0101')]
    # result = result.sort_values(by='stock_code')
    result.reset_index(inplace=True, drop=True)
    result = result.copy()
    columns = ['stock_code', 'start_date', 'end_date', 'start', 'end', 'distance','prev_close', 'close', 'price_percent', 'start_type', 'end_type']
    print("___________________________________________")
    print_result(start_year,end_year,result)
    # print_mtd_result(start_year,end_year,result)
    cache_data = {}
    for index, value in result.iterrows():
        if value['stock_code'] in cache_data.keys():
            item_data = cache_data.get(value['stock_code'])
        else:
            item_data = pd.read_pickle(os.path.join(uscg.pre_pkl, value['stock_code'] + '.pkl'))
            cache_data[value['stock_code']] = item_data
            item_data.reset_index(drop=True, inplace=True)

    df_hs300 = read_hs300()
    MA20_MA60 = []
    for index,value in result.iterrows():
        item_data = cache_data.get(value['stock_code'])
        result.loc[index, 'MA20_MA60_300']  = df_hs300.loc[value['start_date']]['MA20_MA60']
        result.loc[index,'MA20_MA60'] = item_data['MA60'].iloc[value['start']] / item_data['MA20'].iloc[value['start']]*100 - 100

    for i in range(-5,5):
        for j in range(-5, 5):
            print(f"MA: {i}-{i + 1}, HS300: {j}-{j+1}")
            result1 = result[(result['MA20_MA60'] >= i) & (result['MA20_MA60'] < i+1)].copy()
            result2 = result1[(result1['MA20_MA60_300'] >= j) & (result1['MA20_MA60_300'] < j+1)].copy()
            print_result(start_year,end_year,result2)


    # result['MA20_MA60'] = MA20_MA60
    # 计算品种当日比值和HS300的比值的相关系数
    # correlation = result['MA20_MA60'].corr(result['MA20_MA60_300'])

    # print("品种当日比值和HS300的比值的相关系数:", correlation)

    from pyecharts.charts import Bar
    from pyecharts import options as opts

    # 定义区间范围和对应的计数器
    interval_ranges = [(x,x+1) for x in range(-8,30)]
    counter = [0] * len(interval_ranges)

    # 统计每个区间内出现的数据个数
    for index,value in result.iterrows():
        for i, (start, end) in enumerate(interval_ranges):
            if start <= float(value['price_percent']) < end:
                counter[i] += 1
                break

    counter = [f'{(x/len(result)*100):.2f}' for x in counter]
    over_product = 0
    for i,p in enumerate(counter):
        if i > 10:
            over_product += float(p)*(i-10)

    # Pyecharts 绘制柱状图
    bar = (
        Bar()
            .add_xaxis([f"{start}-{end}%" for start, end in interval_ranges])
            .add_yaxis("百分比统计", counter)
            .set_global_opts(title_opts=opts.TitleOpts(title="百分比数据区间统计"))
    )

    bar.render("percentages_bar_chart.html")

    distance_list = result['distance'].to_list()

    from collections import Counter
    counter = Counter(distance_list)

    # 提取数据
    numbers = sorted(list(counter.keys()))
    counts = [counter[x] for x in numbers]
    from pyecharts.charts import Bar
    from pyecharts import options as opts
    bar2 = (
        Bar()
            .add_xaxis(numbers)
            .add_yaxis("距离出现次数", counts)
            .set_global_opts(title_opts=opts.TitleOpts(title="分型间隔距离统计图"))
    )
    # 渲染图表
    bar2.render(f"../html/pattern_distances_bar_chart.html")

    # stat_3day(result)

if __name__ == '__main__':
    # stat_txt()
    from prettytable import PrettyTable
    data = []
    # print_table(data)
    #
    # stat_add_pkl(2019,2024)
    # for x in range(2019,2024):
    #     stat_add_pkl(x, x + 1)


    stat_pkl(2016,2024)
    for x in range(2016,2024):
        stat_pkl(x,x+1)

    # stat_hs300(2019,2024)
    # stat_hs300(2018,2024)
    # for x in range(2017,2024):
    #     print(x)
    #     stat_hs300(x,x+1)



