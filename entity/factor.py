## 创建一个股票指标的因子模型，包括来源的pd原始数据，指标的计算公式，以及因子暴露的计算公式
import pandas as pd

import user_config as uscg
from func_TDX import *
import os
import talib
import numpy as np
import statistics
import sys
import multiprocessing, time


class BaseFactorModel:
    def __init__(self, data=None):
        data: pd.DataFrame  # 指定是pd加载的数据
        """
        初始化因子模型，并加载原始数据。
        :param data: 原始数据，包含股票代码、日期和相关的财务指标。
        """
        self.data = data
        self.factor = 1
        self.stockCode = None

    def loadcsv(self,stockCode,start_date='20170101',end_date='20210101'):
        data = pd.read_csv(os.path.join(uscg.csv_lday,stockCode +'.csv'), encoding="GBK")
        if start_date != '':
            if end_date != '':
                data = data[(data['date'] >= start_date) & (data['date'] <= end_date)]
            else:
                data = data[(data['date'] >= start_date)]
            # 重新设置索引从1开始
            data.reset_index(drop=True, inplace=True)
            # data.set_index('date', drop=False, inplace=True)  # 时间为索引。方便与另外复权的DF表对齐合并
        factor = 1
        data['open'] = (data['open'] * factor).round(2)
        data['close'] = (data['close'] * factor).round(2)
        data['low'] = (data['low'] * factor).round(2)
        data['high'] = (data['high'] * factor).round(2)
        data['percent'] = ((data['close'] / REF(data['close'], 1) - 1) * 100).round(2)
        self.data = data
        self.stockCode = stockCode

    def loadpkl(self,stockCode,cache_data=None,start_date='20170101',end_date='20210101'):
        if cache_data is None:
            data = pd.read_pickle(os.path.join(uscg.pre_pkl,stockCode +'.pkl'))
        else:
            data = cache_data
        # 重新设置索引从1开始
        data.reset_index(drop=True, inplace=True)
        self.data = data
        self.stockCode = stockCode

    #设置复权因子，以修正前复权和tdx的显示价格不一致的问题
    def setFactor(self, f):
        self.factor = f

    def plot(self):
        from entity.kline_plot import getGrid
        # 获取历史k线数据
        grid_chart = getGrid(self.data)
        grid_chart.render(f"../html/{self.stockCode}.html")

class StockFactorModel(BaseFactorModel):
    def __init__(self, hs300,lock=None, data=None):
        super().__init__(data)
        self.printlock = lock
        self.hs300 = hs300

    def run_model(self):
        if '流通市值' in self.data.columns:
            if len(self.data) < 1:
                return
            self.stock_value = self.data['流通市值'].iloc[0] / 100000000
            if self.stock_value < 10:
                print(f'{self.stockCode}流通市值{self.stock_value}')
                return

        close = self.data['close']
        percent = self.data['percent']
        from entity.pattern_entity import TopBottomPattern
        pattern = TopBottomPattern(self.data,self.stockCode,self.hs300)
        pattern.hs300 = self.hs300
        pattern.future = False
        pattern.confirm_type = 2
        pattern.run()
        # self.calc_profit_loss_ratio(pattern.buy_cell_segment)
        # self.plot()
        # self.calc_profit_in_day()
        # pattern.plot()
        # print(signal_list[-60:])
        return pattern.buy_cell_segment

    def calc_profit_in_day(self):
        account = pd.DataFrame(columns=['start', 'end', 'distance','prev_close','close', 'price_percent', 'start_type', 'end_type'])
        trade_history = []
        trade = []
        for index,item in self.data.iterrows():
            if item['celue_buy']:
                trade.append(index)

            ## 止损打开
            # if item['close'] - self.data.iloc[trade[0]]['close']:
            #     pass
            ##卖出结束交易
            if item['celue_sell']:
                trade.append(index)
                trade_history.append(trade)
                trade = []

        trade_df = pd.DataFrame()
        for trade in trade_history:
            new_data = {}
            new_data['start'] = trade[0]
            new_data['end'] = trade[-1]
            new_data['distance'] = trade[-1] - trade[0]
            prex = trade[0:-1]
            new_data['prev_close'] = self.data.iloc[prex]['close'].mean()
            new_data['close'] = self.data.iloc[trade[-1]]['close']
            new_data['price_percent'] = (new_data['close'] / new_data['prev_close']).round(2)
            new_data['start_type'] = -1
            new_data['end_type'] = 1
            new_data['buy_count'] = len(trade)
            new_data = {k: [v] for k, v in new_data.items()}
            df = pd.DataFrame(new_data)
            # trade_df.merge(df)

        # print(trade_df)
        pass

    """
        回测数据,计算盈亏比
        连接线段的开始和结束索引,计算区间的收盘价格
    """
    def combine_buy_point(self,segment,buy_count = None):
        segment['buy_count'] = 1
        #合并连续的中继底分型，不断抄底，后续一次性卖出
        combine_start_end_index=[]
        combine_start_end_list=[]
        for index, item in segment.iterrows():
            combine_start_end_index.append(index)
            if item['end_type'] == 1:
                combine_start_end_list.append(combine_start_end_index)
                combine_start_end_index=[]

        data = []
        for combine_start_end_index in combine_start_end_list:
            if buy_count is not None and len(combine_start_end_index) > buy_count:
                continue
            # 新的一行数据
            # 使用loc方法将新行添加到DataFrame的末尾

            # ['start', 'end', 'distance', 'prev_close', 'close', 'price_percent', 'start_type', 'end_type']
            new_data = {}
            new_data['start'] = segment.iloc[combine_start_end_index[0]]['start']
            new_data['end']   = segment.iloc[combine_start_end_index[-1]]['end']
            new_data['distance'] = segment.iloc[combine_start_end_index]['distance'].sum()
            prex = combine_start_end_index[0:-1]
            new_data['prev_close'] = segment.iloc[prex]['close'].mean()
            new_data['close'] = segment.iloc[combine_start_end_index[-1]]['close']
            new_data['price_percent'] = (new_data['close'] / new_data['prev_close']).round(2)
            new_data['start_type'] = segment.iloc[combine_start_end_index[0]]['start_type']
            new_data['end_type'] = segment.iloc[combine_start_end_index[-1]]['end_type']
            new_data['buy_count'] = len(combine_start_end_index)
            new_data = {k:[v] for k,v in new_data.items()}
            c = pd.DataFrame(new_data)
            data.append(c)
        data = pd.concat(data)
        dtype_dict = {'start': 'int','end': 'int','distance': 'int', 'start_type': 'int','end_type': 'int','buy_count': 'int'}
        data = data.astype(dtype_dict)

        # buy_count = [len(x) for x in combine_start_end_list]
        # from collections import Counter
        # buy_count = Counter(buy_count)
        # print("买点次数:"+str(buy_count))
        return data

    def calc_profit_loss_ratio(self,segment):
        # segment['price_percent_abs'] = [abs(x) for x in segment['price_percent']]
        # 对每个分组应用sum函数
        # grouped_sum = statistics_segement_price['price_percent_abs'].mean()
        # 统计顶底分型之间的和收益的关系
        # segment = self.combine_buy_point(segment)
        statistics_segement_price2 = segment.groupby(['start_type', 'end_type'])
        data_split = []
        # data_split.append(((0,0),segment))
        data_split.extend([(index,x) for index,x in statistics_segement_price2])
        for key,data_series in statistics_segement_price2:
            i = 0
            profit = 0
            profit_list = []
            loss_list = []
            loss = 0
            days = []
            for index, item in data_series.iterrows():
                if item['start_type'] == -1:
                    price_percent = item['price_percent']
                    days.append(item['distance'])
                    # if price_percent < -5:
                    #     price_percent = -5
                    if (price_percent > 0):
                        i = i + 1
                        profit = profit + price_percent
                        profit_list.append(price_percent)
                    else:
                        loss = loss + price_percent
                        loss_list.append(price_percent)

            profit = abs(profit)
            loss = abs(loss)
            total = len(data_series)
            if key == (-1,1):
                name = '底分型   '
            elif key == (-1,-1):
                name = '中继底分型'
            else:
                name = "全部"
            if total == 0:
                continue
            # self.printlock.acquire()
            profit_loss_rate = profit / loss if loss != 0 else 0
            avg_profit =  profit / i if i != 0 else 0
            avg_loss = loss / (total - i) if (total - i) != 0 else 0
            result_data = {'date': '', 'end_date': '', 'stock_code': self.stockCode, 'stock_value': self.stock_value,
                           'succ_rate': i / total, 'total': total, 'succ_count': i, 'profit': profit, 'loss': loss,
                           'profit_rate':profit_loss_rate,
                           'avg_day': statistics.mean(days)}
            info1 =(f'{name}，流通市值{self.stock_value:.1f}，{self.stockCode},成功率为{i / total:.3f}，共计次数{total}，成功数量为{i},盈利{profit:.2f}%,亏损{loss:.2f}%'
                  f',盈亏比{profit_loss_rate:.2f},平均天数{statistics.mean(days)},平均盈利{avg_profit:.2f}%,平均亏损{avg_loss:.2f}%')
            print(info1)
            # self.printlock.release()
            # profit_list = sorted(profit_list)
            # variance = profit_list[int(len(profit_list) * 0.68)]
            # loss_list = sorted(loss_list,reverse=True)
            # variance2 = loss_list[int(len(loss_list) * 0.68)]
            # info2 = f'盈利分布,置信度68%:{variance:.2f},平均数:{statistics.mean(profit_list):.2f},中位数:{statistics.median(profit_list)}'
            # self.printlock.acquire()
            # print(info2.txt)
            # self.printlock.release()
            # print(f'亏损分布,置信度68%:{variance2:.2f},平均数:{statistics.mean(loss_list):.2f},中位数:{statistics.median(loss_list)}')
            return result_data

    def plot_bar(self,profit_list):
        # 定义区间和对应的统计计数
        # 设定数值范围的最小值和最大值
        min_value = 0
        max_value = max(profit_list)
        # 计算每个区间的宽度（最大值和最小值的差除以分区数）
        interval_width = (max_value - min_value) / 10
        # 生成每个区间的边界列表
        intervals = [min_value + i * interval_width for i in range(11)]
        intervals_names = [f"[{intervals[i]:.2f}, {intervals[i + 1]:.2f})" for i in range(10)]
        # 初始化每个区间的计数器
        interval_counts = {name: 0 for name in intervals_names}

        # 统计每个区间出现的次数
        for num in profit_list:
            # 找到数字所在的区间
            for i in range(len(intervals) - 1):
                if intervals[i] <= num < intervals[i + 1]:
                    interval_counts[intervals_names[i]] += 1
                    break

        from pyecharts.charts import Bar
        from pyecharts import options as opts

        # 创建柱状图
        bar = Bar()

        # 添加X轴和Y轴的数据
        bar.add_xaxis(intervals_names)  # X轴为区间名称
        bar.add_yaxis("频次", list(interval_counts.values()))  # Y轴为各区间出现次数

        # 设置全局配置项
        bar.set_global_opts(title_opts=opts.TitleOpts(title=f"频次统计,总个数{len(profit_list)}"),
                            toolbox_opts=opts.ToolboxOpts())  # 可选，添加工具箱

        # 渲染图表到文件（也可以直接在Jupyter Notebook中显示）
        bar.render("interval_counts.html")



def paginate_list(input_list, page_size):
    """
    对列表进行分页
    Parameters:
    input_list (list): 要分页的列表
    page_size (int): 每页的大小
    Returns:
    list: 分页后的列表
    """
    paginated_list = []
    for i in range(0, len(input_list), page_size):
        paginated_list.append(input_list[i:i + page_size])
    return paginated_list


def main_work(range_code_type,year,time_str):
    print(f'Worker {range_code_type} is executing')
    df_hs300 = pd.read_csv(uscg.csv_index + '/000300.csv', index_col=None, encoding='gbk', dtype={'code': str})
    df_hs300['date'] = df_hs300['date'].str.replace('-', '')

    # start_date = f'{year-1}0101'
    # end_date = f'{year+1}0301'
    #
    start_date = f'20180101'
    end_date = f'20240301'

    if start_date != '':
        if end_date != '':
            df_hs300 = df_hs300[(df_hs300['date'] >= start_date) & (df_hs300['date'] <= end_date)]
        else:
            df_hs300 = df_hs300[(df_hs300['date'] >= start_date)]
        # 重新设置索引从1开始
        # df_hs300.reset_index(drop=True, inplace=True)

    df_hs300.set_index('date', drop=False, inplace=True)  # 时间为索引。方便与另外复权的DF表对齐合并
    df_hs300['MA5'] = talib.MA(df_hs300['close'], 5)
    df_hs300['MA20'] = talib.MA(df_hs300['close'], 20)
    df_hs300['MA60'] = talib.MA(df_hs300['close'], 60)

    for root, dirs, files in os.walk(uscg.pre_pkl, topdown=False):
        filenumber = [file[:-4] for file in files]
        file_names = paginate_list(filenumber,500)
        cache_data = {}
        for code in file_names[range_code_type]:
            data = pd.read_pickle(os.path.join(uscg.pre_pkl, code + '.pkl'))
            cache_data[code] = data

        buy_cell_detail_list = []
        result = None
        for code,data in cache_data.items():
            m = StockFactorModel(df_hs300)
            m.loadpkl(code,data,start_date,end_date)
            buy_cell_detail = m.run_model()
            if buy_cell_detail is None:
                continue
            if result is None:
                result = buy_cell_detail.copy()
            elif not buy_cell_detail.empty:
                result = pd.concat([result,buy_cell_detail], axis=0)

        # result.to_pickle(f'../result/result_{time_str}_{str(year)}_{range_code_type}.pkl')
        result.to_pickle(f'../result/{time_str}/result_{time_str}_{str(year)}_{range_code_type}.pkl')

if __name__ == '__main__':
    mult_result = []
    import datetime
    time_str = datetime.datetime.now().strftime('%Y%m%d%H%M')
    path = f'../result/{time_str}'
    if not os.path.exists(path):
        os.mkdir(path)

    t = time.time()
    # 定义一个函数，用于被多个进程执行

    # 创建多个进程，每个进程都执行 worker 函数
    # 打开文件以写入模式
    year = 2022
    processes = []
    # main_work(0, year,time_str)

    for i in range(5):  # 假设创建5个进程
        p = multiprocessing.Process(target=main_work, args=(i, year,time_str,))
        processes.append(p)
        p.start()

    # 等待所有进程完成
    for p in processes:
        p.join()

    import shutil
    # 复制文件
    shutil.copy("pattern_entity.py", os.path.join(path,"pattern_entity.py"))
    print(f'运行时间:{time.time()-t}s')