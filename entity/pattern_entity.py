#创建一个顶底分型列表的实体类
# 定义实体类，数据包括，顶底分型的列表，顶底分型的距离和出现次数，顶底分型的连接的线段的开始和结束索引
# future定义这个指标是否是未来函数，表示新的分型出现后，旧的同方向分型是否因此消失
# confirm定义这个指标是否是确认函数，表示顶底分型延续确认，四条k线组成
import pandas as pd
import talib
from func_TDX import *
import math
# from ..user_config import strategy_params
class TopBottomPattern:
    def __init__(self,data,stockCode,hs300):
        assert isinstance(data,pd.DataFrame)
        self.data = data
        self.stockCode = stockCode
        self.future = False
        self.confirm_type = 0
        self.hs300 = hs300
        # 顶底分型列表，每个元素是一个包含顶底价格的元组 (top_price, bottom_price)
        # 连接线段的开始和结束索引列表，每个元素是一个包含开始和结束索引的元组 (start_index, end_index,distence,price_percent,start_type,end_type)
        self.buy_cell_segment = []
        self.buy_sell_list = []
        # 顶底分型的距离列表，每个元素是一个顶底之间的距离
        self.distance_list = []

    def run(self):
        self.add_confirm_pattern(self.data)
        self.calculate_pattern_distances()
        # self.data.to_pickle(f"{self.stockCode}.pkl")
        return self.buy_cell_segment

    ##
    # 顶底分型列表，1:顶分型,-1底分型,0没有分型
    # 顶底分型进行统计和计算，计算每个分型的间隔距离，保存一个列表
    # 统计每种距离出现的总次数

    def func_down_cross_5d(self,i,close,MA5):
        #统计前1-2-3根k线不能高于MA5均线下穿，一买调整不能少于3天
        # 牛市不需要，熊市加上效果明显，避免频繁震荡，只加一个过滤，可在熊市和牛市见平衡
        # return True
        return (close.iloc[i-2] < MA5.iloc[i-2])
        # return (close.iloc[i-2] < MA5.iloc[i-2]) & (close.iloc[i-3] < MA5.iloc[i-3])

    def cross(self,close,MA5, i,limit=0.01):
        result1 = (close.iloc[i - 1] < MA5.iloc[i - 1]) & ((close.iloc[i]   - MA5.iloc[i])   / (MA5.iloc[i]) >= limit)
        result2 = (close.iloc[i - 2] < MA5.iloc[i - 2]) & ((close.iloc[i-1] - MA5.iloc[i-1]) / (MA5.iloc[i-1]) < limit) & ((close.iloc[i] - MA5.iloc[i]) / (MA5.iloc[i]) > limit)
        return  result1 or result2


    def ma_signal2(self,i,data,MA5,MA10,MA20,MA60,buy=True):
        close = data['close']
        percent = data['percent']
        # & self.func_down_cross_5d(i, close, MA5) \
        bottom_signal = self.cross(close,MA5,i)  & self.func_down_cross_5d(i, close, MA5) \
                        & (MA5.iloc[i] > MA20.iloc[i]) & (percent.iloc[i] > 2.5) & (percent.iloc[i] < 7) \
                        & ((close.iloc[i] - MA20.iloc[i]) / MA20.iloc[i] < 0.05) \
                        & (( MA60.iloc[i] - MA20.iloc[i]) / MA20.iloc[i] < self.hs300_param0) & ((MA60.iloc[i] - MA20.iloc[i]) / MA20.iloc[i] > self.hs300_param1)
        top_signal = (close.iloc[i] < MA10.iloc[i]) & (close.iloc[i-1] > MA10.iloc[i-1])
        if buy:
            return bottom_signal
        else:
            return top_signal

    def hs300_signal(self,i,MA5,MA20,MA60,CLOSE):
        # if REF_MA5.shape[0] != MA5.shape[0]:
        #     print("长度不匹配，不做参考")
        # return True
        # else:
        param = (MA60.loc[i] / MA20.loc[i]) - 1
        if param > 0 and param < 0.02:
            return False
        return ~((MA5.loc[i] < MA20.loc[i]) & (MA20.loc[i] < MA60.loc[i]) & (CLOSE.loc[i] < MA5.loc[i]))


    ## 根据目前的大环境,动态调整牛市和熊市参数
    ## 熊市,基本上就是0.05 0
    ## 大牛市,基本上就是0.02 -0.05
    ## 慢牛市,基本上就是0.05 -0.02
    def hs300_MA60_signal(self,i,MA20,MA60):
        #最佳参数
        return 0.05, -0.02

    def add_confirm_pattern(self,data):
        ##1:顶分型,-1底分型,0无分型
        # open = data['open']
        close = data['close']
        # high = data['high']
        # low = data['low']
        percent = data['percent']
        vol = data['vol']//10000
        self.buy_sell_list = []
        last_signal = 0
        MA5 = data['MA5']
        MA10 = data['MA10']
        MA20 = data['MA20']
        MA60 = data['MA60']
        VOL100 = data['VOL100']
        CLOSE_HS = self.hs300['close']
        MA5_HS = self.hs300['MA5']
        MA20_HS = self.hs300['MA20']
        MA60_HS = self.hs300['MA60']
        if data['close'].iloc[-1] < 7:
            # print(f"{data['code'].iloc[0]:06d}排除4元以下，不做参考")
            return

        no_hs = MA5_HS.shape[0] != MA5.shape[0]
        # if no_hs:
            # print(f"{data['code'].iloc[0]:06d}长度不匹配，不做参考")
            # return
        # no_hs = True
        buy_price = 0
        #止损位5个点
        stop_and_sell_percent = 0.03
        buy_index = 0
        add_index = 0
        bottom_signal_list = []
        top_signal_list = []
        for i, item in self.data.iterrows():
            if i < 2:
                self.buy_sell_list.append(0)
                bottom_signal_list.append(False)
                top_signal_list.append(False)
                continue
            self.hs300_param0,self.hs300_param1 = self.hs300_MA60_signal(data['date'].iloc[i], MA20_HS, MA60_HS)
            bottom_signal = (self.ma_signal2(i,data,MA5,MA10,MA20,MA60,True) &(vol.iloc[i] > VOL100.iloc[i] * 1.1) \
                             & self.hs300_signal(data['date'].iloc[i], MA5_HS, MA20_HS, MA60_HS, CLOSE_HS))
            top_signal_5 = (close.iloc[i] < MA5.iloc[i]) & (close.iloc[i-1] > MA5.iloc[i-1])

            if top_signal_5 and last_signal == -1:
                skip_5_down = True
                pass
            if bottom_signal:
                self.buy_sell_list.append(-1)
                last_signal = -1
                buy_price = data['close'].iloc[i]
                bottom_signal_list.append(True)
                top_signal_list.append(False)
            elif last_signal == -1:
                if(buy_price - data['close'].iloc[i]> buy_price * stop_and_sell_percent):
                    last_signal = self.add_sell_sign(bottom_signal_list, last_signal, top_signal_list)
                elif top_signal_5 :
                    last_signal = self.add_sell_sign(bottom_signal_list, last_signal, top_signal_list)
                # elif top_signal_10:
                #     last_signal = self.add_sell_sign(bottom_signal_list, last_signal, top_signal_list)
                else:
                    self.buy_sell_list.append(0)
                    bottom_signal_list.append(False)
                    top_signal_list.append(False)
            else:
                self.buy_sell_list.append(0)
                bottom_signal_list.append(False)
                top_signal_list.append(False)

        self.data['celue_buy'] = bottom_signal_list
        self.data['celue_sell'] = top_signal_list

    def add_sell_sign(self, bottom_signal_list, last_signal, top_signal_list):
        self.buy_sell_list.append(1)
        last_signal = 1
        bottom_signal_list.append(False)
        top_signal_list.append(True)
        return last_signal

    def future_patterns(self,pattern_list):
        updated_list = []
        last_pattern = 0
        last_pattern_index = 0
        for index, pattern in enumerate(pattern_list):
            if pattern != 0:
                # 如果当前分型与上一个分型方向相同，则将上一个分型改为0
                if pattern == last_pattern and last_pattern != 0:
                    updated_list[last_pattern_index] = 0
                updated_list.append(pattern)
                last_pattern = pattern
                last_pattern_index = index
            else:
                updated_list.append(pattern)
        return updated_list

    def findNotZero(self,signal_list):
        assert isinstance(signal_list, list)
        for i in range(len(signal_list) - 1, 0, -1):
            if signal_list[i] != 0:
                return signal_list[i]
        return 0

    def calculate_pattern_distances(self):
        """
         计算顶底分型列表中各个分型之间的间隔距离，并统计每种距离出现的总次数。
         参数：
         pattern_list (list): 包含顶底分型的列表，1 表示顶分型，-1 表示底分型，0 表示没有分型。
         返回：
         dict: 包含每种距离和出现次数的字典。
       """
        distances = []
        prev_index = None
        prev_value = None
        segment_indices = []
        # 遍历列表，找到相邻的顶分型或底分型
        for index, value in enumerate(self.buy_sell_list):
            if value in [1, -1]:
                if prev_index is not None and prev_value == -1:
                    # 必须是买点的，计算距离并添加到列表中
                    distance = index - prev_index  # 计算当前分型与上一个分型之间的距离
                    distances.append(distance)
                    close = self.data.loc[index]['close']
                    prev_close = self.data.loc[prev_index]['close']
                    price_percent = ((close - prev_close) * 100.0 / prev_close).round(2)
                    start_date = self.data.loc[prev_index]['date']
                    end_date = self.data.loc[index]['date']
                    segment_indices.append((self.stockCode,start_date,end_date,prev_index, index, distance,prev_close,close, price_percent,prev_value,value,))
                prev_index = index  # 更新上一个分型的索引
                prev_value = value  # 更新上一个分型的类型
        self.buy_cell_segment = pd.DataFrame(segment_indices, columns=['stock_code','start_date','end_date','start', 'end', 'distance','prev_close','close','price_percent','start_type','end_type'])
        return self.buy_cell_segment


    def get_distance_list(self):
        # 提取距离和出现次数
        distance_list = sorted(list(self.distance_counts.keys()))
        return distance_list
    def getOccuranceCount(self):
        # 顶底分型出现的次数
        occurance_count = [self.distance_counts[k] for k in self.get_distance_list()]
        return occurance_count

    def plot(self):
        from pyecharts.charts import Bar
        from pyecharts import options as opts
        bar = (
            Bar()
            .add_xaxis([str(distance) for distance in self.distance_list])
            .add_yaxis("距离出现次数", self.occurance_count)
            .set_global_opts(title_opts=opts.TitleOpts(title="分型间隔距离统计图"))
        )
        # 渲染图表
        bar.render(f"../html/{self.stockCode}_pattern_distances_bar_chart.html")

    def get_pattern_list(self):
        """
        获取顶底分型列表
        """
        return self.buy_sell_list

    def get_segment_indices(self):
        """
        获取连接线段的开始和结束索引列表
        """
        return self.buy_cell_segment

def future_patterns(pattern_list):
    updated_list = []
    last_pattern = 0
    last_pattern_index = 0
    for index, pattern in enumerate(pattern_list):
        if pattern != 0:
            # 如果当前分型与上一个分型方向相同，则将上一个分型改为0
            if pattern == last_pattern and last_pattern != 0:
                updated_list[last_pattern_index] = 0
            updated_list.append(pattern)
            last_pattern = pattern
            last_pattern_index = index
        else:
            updated_list.append(pattern)
    return updated_list

if __name__ == '__main__':
    """
    
    """
    pass