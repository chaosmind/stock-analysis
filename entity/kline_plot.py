# 绘制K线图 & MACD & MA

import datetime
from pyecharts import options as opts
from pyecharts.charts import Kline, Line, Bar, Grid
import numpy as np
from pyecharts.commons.utils import JsCode
import talib
import pandas as pd
from pyecharts.globals import ThemeType

import user_config as uscg
FUTUOPEND_ADDRESS = '127.0.0.1'  # FutuOpenD 监听地址
FUTUOPEND_PORT = 11111  # FutuOpenD 监听端口

TRADING_CODE = 'HK.00700'  # 股票标的
START_DATE = '2018-09-11'  # 股票回溯开始日期
END_DATE = datetime.date.today().strftime('%Y-%m-%d')  # 股票回溯结束日期
FAST_MOVING_AVERAGE   = 5  # 均线快线的周期
MIDDLE_MOVING_AVERAGE = 20  # 均线快线的周期
SLOW_MOVING_AVERAGE   = 60  # 均线慢线的周期

# MACD相关设定
MACD_SHORT = 12
MACD_LONG = 26
MACD_SIGNAL = 9

grid_start_height = [0,"62.5%","75%"]
# 根据收盘价，计算MA, 返回快线和慢线数组
def calculate_MA(closeArray, fast_param, slow_param, mid_param):
    fast_values = []  # 快线数组
    mid_values = []   # 中线数组
    slow_values = []  # 慢线数组

    for i in range(len(closeArray)):
        if i < FAST_MOVING_AVERAGE:
            fast_values.append(np.nan)
        else:
            fast_value = format(sum(closeArray[i - FAST_MOVING_AVERAGE:i]) / FAST_MOVING_AVERAGE,
                                '.2f')  # 四舍五入取2位小数
            fast_values.append(float(fast_value))

        if i < MIDDLE_MOVING_AVERAGE:
            mid_values.append(np.nan)
        else:
            mid_value = format(sum(closeArray[i - MIDDLE_MOVING_AVERAGE:i]) / MIDDLE_MOVING_AVERAGE, '.2f')
            mid_values.append(float(mid_value))

        if i < SLOW_MOVING_AVERAGE:
            slow_values.append(np.nan)
        else:
            slow_value = format(sum(closeArray[i - SLOW_MOVING_AVERAGE:i]) / SLOW_MOVING_AVERAGE, '.2f')
            slow_values.append(float(slow_value))

    return fast_values, slow_values, mid_values


def calculateMACD(closeData, shortPeriod=12, longPeriod=26, signalPeriod=9):
    # 这里手动计算结果和通达信的不正确
    macd, signal, hist = talib.MACD(closeData, fastperiod=12, slowperiod=26, signalperiod=9)
    return macd.round(2), signal.round(2), hist.round(2)


# 画k线图
def draw_klines(data, self=None):
    klineData = data[['open', 'close', 'low', 'high','percent']].values.tolist()  # 将pd转换为数组 股价
    klineX = data['date'].values.tolist()
    klineXData = [dateStr[0:10] for dateStr in klineX]  # 横坐标 日期
    # mark_line_data = getMarkLineData(data, klineData)
    kline = Kline()
    kline.add_xaxis(xaxis_data=klineXData)
    kline.add_yaxis(
        series_name="日k线",
        y_axis=klineData,
        itemstyle_opts=opts.ItemStyleOpts(opacity=.8),
        markline_opts=opts.MarkLineOpts(
            # data=mark_line_data,
            linestyle_opts=opts.LineStyleOpts(width=2, color="yellow"),
        ),
    )
    kline.set_global_opts(
        xaxis_opts=opts.AxisOpts(
            name='日期',
            is_scale=True,
        ),
        yaxis_opts=opts.AxisOpts(
            name='股价',
            is_scale=True,
            splitarea_opts=opts.SplitAreaOpts(
                is_show=True,
                areastyle_opts=opts.AreaStyleOpts(opacity=0.7)
            ),
        ),
        datazoom_opts=[
            opts.DataZoomOpts(
                is_show=False,
                type_="inside",
                xaxis_index=[0, 1, 2],
                range_start=80,
                range_end=100,
            ),
            # xaxis_index=[0, 0]设置第一幅图为内部缩放
            opts.DataZoomOpts(
                is_show=True,
                type_="slider",
                xaxis_index=[0, 1, 2],
                pos_top="95%",
                range_start=80,
                range_end=100,
            ),
            # xaxis_index=[0, 1]连接第二幅图的axis
        ],
        title_opts=opts.TitleOpts(title="股价k线图"),
        tooltip_opts=opts.TooltipOpts(trigger='axis', axis_pointer_type='cross'),
    )
    # 计算涨跌比
    # change_ratio = [(data[i][1] - data[i][1]) / data[i][1] * 100 for i in range(len(data))]
    tooltip_formatter = """
    <b>K线图</b>
    <br/>开盘价: {params[0]['data'][2]}
    <br/>收盘价: {@close}
    <br/>最低价: {@low}
    <br/>最高价: {@high}
    <br/>百分比: {df['percent'].iloc[params[0]['dataIndex']]:.2f}%
    """
    # 将涨跌比添加到 tooltip 中
    # kline.set_series_opts(tooltip_opts=opts.TooltipOpts(formatter=tooltip_formatter))

    maLine = Line()
    maLine.add_xaxis(klineXData)
    closeArray = data['close'].values.tolist()
    fast_MA, slow_MA, mid_MA = calculate_MA(closeArray, FAST_MOVING_AVERAGE, SLOW_MOVING_AVERAGE,MIDDLE_MOVING_AVERAGE)  # 计算均线

    maLine.add_yaxis(
        series_name="MA" + str(FAST_MOVING_AVERAGE),
        y_axis=fast_MA,
        is_smooth=True,
        linestyle_opts=opts.LineStyleOpts(opacity=0.9, width=2),
        label_opts=opts.LabelOpts(is_show=False),  # 不显示具体数值
        z=3,  # ma在最顶层
    )

    maLine.add_yaxis(
        series_name="MA" + str(MIDDLE_MOVING_AVERAGE),
        y_axis=mid_MA,
        is_smooth=True,
        linestyle_opts=opts.LineStyleOpts(opacity=0.9, width=2),
        label_opts=opts.LabelOpts(is_show=False),
        z=4,
    )

    maLine.add_yaxis(
        series_name="MA" + str(SLOW_MOVING_AVERAGE),
        y_axis=slow_MA,
        is_smooth=True,
        linestyle_opts=opts.LineStyleOpts(opacity=0.9, width=2),
        label_opts=opts.LabelOpts(is_show=False),
        z=5,
    )

    KlineWithMA = kline.overlap(maLine)  # MA画在k线上
    return KlineWithMA


def getMarkLineData(data, klineData):
    mark_pos = data['celue_buy'] | data['celue_sell']
    distances = []
    prev_index = None
    # 遍历列表，找到相邻的顶分型或底分型
    for index, value in enumerate(mark_pos):
        if value:
            if prev_index is not None:
                distances.append((prev_index, index))
            prev_index = index  # 更新上一个分型的索引
    mark_line_data = []
    for idx_start, idx_end in distances:
        mark_line_data.append(
            [
                {
                    "xAxis": idx_start,
                    "yAxis": klineData[idx_start][1],
                },
                {
                    "xAxis": idx_end,
                    "yAxis": klineData[idx_end][1],
                },
            ]
        )
    return mark_line_data


# 画MACD
def drawMACD(data):
    DIF, DEA, MACD = calculateMACD(data['close'], MACD_SHORT, MACD_LONG, MACD_SIGNAL)

    klineX = data['date'].values.tolist()
    klineXData = [dateStr[0:10] for dateStr in klineX]

    bar_2 = Bar()
    bar_2.add_xaxis(klineXData)
    bar_2.add_yaxis(
        series_name="MACD",
        y_axis=MACD.tolist(),
        xaxis_index=2,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
        z=0,
        itemstyle_opts=opts.ItemStyleOpts(
            opacity=0.7,
            color=JsCode(
                # 就是这么神奇，要写在注释中,但是的确是生效的,通过浏览器进行解析
                """
                function(params) {
                    var colorList;
                    if (params.data >= 0) {
                      colorList = '#D3403B';
                    } else {
                      colorList = '#66A578';
                    }
                    return colorList;
                }
                """
            )
        )
    )

    bar_2.set_global_opts(
        xaxis_opts=opts.AxisOpts(
            type_="category",
            grid_index=2,
            axislabel_opts=opts.LabelOpts(is_show=True),
            is_scale=True,
        ),
        yaxis_opts=opts.AxisOpts(
            grid_index=2,
            split_number=4,
            axisline_opts=opts.AxisLineOpts(is_on_zero=False),
        ),

        legend_opts=opts.LegendOpts(is_show=True, pos_top=grid_start_height[2]),  # 图例
    )

    line_2 = Line()
    line_2.add_xaxis(klineXData)
    line_2.add_yaxis(
        series_name="DIF",
        y_axis=DIF,
        xaxis_index=1,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
        linestyle_opts=opts.LineStyleOpts(opacity=1.0, width=2),
    )
    line_2.add_yaxis(
        series_name="DEA",
        y_axis=DEA,
        xaxis_index=1,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
        linestyle_opts=opts.LineStyleOpts(opacity=1.0, width=2),
    )

    overlap_bar_line = bar_2.overlap(line_2)
    return overlap_bar_line


# 画成交量柱
def drawVOL(data):
    # closeArray = data['vol'].values.tolist()
    closeArray = (data['vol'] / 1000_000).round(1)
    SMA5 = talib.SMA(closeArray,5).round(1)
    SMA100 = talib.SMA(closeArray,100).round(1)

    klineX = data['date'].values.tolist()
    klineXData = [dateStr[0:10] for dateStr in klineX]
    up   = [(round(inc['vol'] / 1000_000,2) if inc['close'] >= inc['open'] else 0) for i,inc in data.iterrows()]
    down = [(round(inc['vol'] / 1000_000,2) if inc['close'] < inc['open'] else 0) for i,inc in data.iterrows()]
    bar_2 = Bar()
    bar_2.add_xaxis(klineXData)
    bar_2.add_yaxis(
        series_name="上涨成交量/万手",
        y_axis=up,
        xaxis_index=1,
        yaxis_index=1,
        label_opts=opts.LabelOpts(is_show=False),
        z=0,
        stack="stack1",
        itemstyle_opts=opts.ItemStyleOpts(opacity=0.8,color='#ef232a')
    )
    bar_2.add_yaxis(
        series_name="下跌成交量/万手",
        y_axis=down,
        xaxis_index=1,
        yaxis_index=1,
        label_opts=opts.LabelOpts(is_show=False),
        z=0,
        stack="stack1",
        itemstyle_opts=opts.ItemStyleOpts(opacity=0.8,color='#14b143')
    )
    bar_2.set_global_opts(
        xaxis_opts=opts.AxisOpts(
            type_="category",
            grid_index=1,
            axislabel_opts=opts.LabelOpts(is_show=True),
            is_scale=True,
        ),
        yaxis_opts=opts.AxisOpts(
            grid_index=1,
            split_number=4,
            axisline_opts=opts.AxisLineOpts(is_on_zero=False),
        ),

        legend_opts=opts.LegendOpts(is_show=True, pos_top=grid_start_height[1]),  # 图例, 显示每种线条对应的名称和颜色
    )

    line_2 = Line()
    line_2.add_xaxis(klineXData)
    line_2.add_yaxis(
        series_name="MA5",
        y_axis=SMA5,
        xaxis_index=1,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
        linestyle_opts=opts.LineStyleOpts(opacity=1.0, width=2),
    )
    line_2.add_yaxis(
        series_name="MA100",
        y_axis=SMA100,
        xaxis_index=1,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
        linestyle_opts=opts.LineStyleOpts(opacity=1.0, width=2),
    )

    overlap_bar_line = bar_2.overlap(line_2)
    return overlap_bar_line


def drawAll(overlap_kline_ma, overlap_bar_line,overlap_bar_macd):
    grid_chart = Grid(init_opts=opts.InitOpts(width="2560px", height="1200px",theme=ThemeType.DARK))
    # 这个是为了把 data.datas 这个数据写入到 html 中,还没想到怎么跨 series 传值
    # demo 中的代码也是用全局变量传的
    # grid_chart.add_js_funcs("var barData = {}".format(data))

    # K线图和 MA 的折线图
    grid_chart.add(
        overlap_kline_ma,
        grid_opts=opts.GridOpts(pos_left="3%", pos_right="3%", height="55%"),
    )
    # VOL
    grid_chart.add(
        overlap_bar_line,
        grid_opts=opts.GridOpts(pos_left="3%", pos_right="3%", pos_top=grid_start_height[1], height="10%"),
    )
    # MACD DIFS DEAS
    grid_chart.add(
        overlap_bar_macd,
        grid_opts=opts.GridOpts(pos_left="3%", pos_right="3%", pos_top=grid_start_height[2], height="15%"),
    )
    return grid_chart


def getGrid(kdata):
    k = draw_klines(kdata)
    m = drawMACD(kdata)
    v = drawVOL(kdata)
    return drawAll(k, v, m)

# 主函数
if __name__ == '__main__':
    from entity.factor import *
    # 读取CSV文件
    df_hs300 = pd.read_csv(uscg.csv_index + '/000300.csv', index_col=None, encoding='gbk', dtype={'code': str})
    df_hs300['date'] = df_hs300['date'].str.replace('-', '')
    df_hs300.set_index('date', drop=False, inplace=True)  # 时间为索引。方便与另外复权的DF表对齐合并
    df_hs300['MA5'] = talib.MA(df_hs300['close'], 5)
    df_hs300['MA20'] = talib.MA(df_hs300['close'], 20)
    df_hs300['MA60'] = talib.MA(df_hs300['close'], 60)


    m = StockFactorModel(df_hs300)
    m.setFactor(1)
    m.loadpkl("002356")
    m.run_model()
    # 获取历史k线数据
    grid_chart = getGrid(m.data)
    grid_chart.render("../html/002356.html")


